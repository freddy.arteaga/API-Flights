# API MOBILE para vuelos

La aplicacion de vuelos es una API Rest en Node.js  

## Getting Started

Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

## Prerequisites

Se debe tener instalado nodejs y typescript


## Installing

Pasos para poder ejecutar el proyecto:

1) Entrar a la carpeta del proyecto

`cd API-FLIGHTS`

2) Luego de ello ejecutar la instalación de dependencias

`npm install`

3) Se ejecuta el transpilador de typescript y se ejecuta en modo desarrollador

`$ npm run build`
`$ npm run dev`



## Versioning

1.0.0

## Author

- **Freddy Arteaga**
