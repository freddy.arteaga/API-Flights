import { Controller } from '../Controllers'

export const Routes: { [index: string]: any } = {
    Home: {
        verb: 'get',
        uri: '/',
        action: Controller.home
    },
    Flights: {
        verb: 'get',
        uri: '/flights',
        action: Controller.flights
    },
    Flight: {
        verb: 'get',
        uri: '/flight/:id',
        action: Controller.flight
    },
    FlightRound: {
        verb: 'get',
        uri: '/flight',
        action: Controller.findFlights
    },
    flightRoundTrip: {
        verb: 'get',
        uri: '/flight-goback',
        action: Controller.flightRoundTrip
    },
    multiDestination: {
        verb: 'get',
        uri: '/flight',
        action: Controller.multiDestination
    },
    createFlight: {
        verb: 'post',
        uri: '/flight',
        action: Controller.createFlight
    }
}