process.env.NODE_CONFIG_DIR = `${__dirname}/env`;
import { InfraWeb } from './Core/infra-web'
import { middleware } from './Middlewares'
import { Routes } from './routes'
import { Connect } from './Config/Connection'

export default class Server extends InfraWeb {
    constructor() {
        super()
        Connect()
        this.use(middleware)
        this.mountRoutes(Routes)
    }
    public static bootstrap(): Server {
        return new Server()
    }
}
Server.bootstrap().startServer()