export interface ISites {
    iata: string,
    name: string,
    airport: string,
    time: string
}