import { Flight } from '../models/Schema/Flights'
export const Services = {
    home: async () => {
        return 'Service Home'
    },
    flights: async () => {
        try {
            const data = await Flight.find()
            return data
        } catch (error) {
            return `Error al buscar los Vuelos ${error}`
        }
    },
    flight: async (idFlight: any) => {
        try {
            const data = await Flight.findById(idFlight)
            return data
        } catch (error) {
            return `Error al buscar el vuelo ${error}`
        }
    },
    saveFlight: async (flight: any) => {
        try {
            const data = await Flight.create(Object.assign({}, flight))
            return data
        } catch (error) {
            return `Error al crear el vuelo ${error}`
        }
    },
    OneFlight: async (params: any) => {
        try {
            const { destiny, origin, date } = params
            console.log('params', params)

            const data = await Flight.find({
                'destination.iata': destiny,
                'origin.iata': origin,
                'date': new Date(date)
            })

            return data
        } catch (error) {
            return error
        }
    },
    Round: async (params: any) => {
        try {
            const { origin, destination } = params
            const going = await Services.flight(
                { origin: origin, destination: destination })
            const backing = await Services.flight(
                { origin: destination, destination: origin })
            
            return { goes: going, backend: backing }
        } catch (error) {
            return `Error al consultar vuelos ida y vuelta ${error}`
        }
    },
    Multi: async (params: any) => {
        try {
            const { trips } = params
            let multiFlights: any = {}
            for (let i = 0; i < trips.length; i++) {
                multiFlights[`Flight${i + 1}`] = await Services.flight(trips[i])
            }
            return multiFlights
        } catch (error) {
            return `Error al consultar vuelos multiples ${error}`
        }
    }
}