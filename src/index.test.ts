import express from 'express';
import bodyParser from 'body-parser';
import request from 'supertest';
import { Routes } from './routes';
import { middleware } from './middlewares'

describe('Loading server express', () => {
    const app = express();

    Object.keys(Routes).forEach((key) => {
        app[Routes[key].verb](Routes[key].uri, Routes[key].action);
    });

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json())


    const objFlight = 
        {
            "price": 121212,
            "_id": "5add25b6e4a085605a82dc81",
            "destination": "Colombia",
            "image": "https://www.elconfidencial.com/tecnologia/2017-04-06/whatsapp-vuelos-internet-ahorro-billetes-avion-semana-santa_1361645/",
            "segments": "ni idea",
            "createdAt": "2018-04-23T00:15:50.411Z",
            "updatedAt": "2018-04-23T00:15:50.411Z"
        }
    

    it('It responds to /flight', () => {
        return request(app).get('/').then((res) => expect(200).toEqual(res.status));
    });
    it('It responds to /home', () => {
        return request(app).get('/').then((res) => expect("Home").toEqual(res.text));
    });



    it('It get customer by id', () => {
         return request(app).get('/flights').then((res) => expect(200).toEqual(res.status));
    });

})