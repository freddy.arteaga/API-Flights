import { Services } from '../Services'
import { Response, Request } from 'express'

export const Controller = {
    home: async (req: Request, res: Response) => {

        const data = await Services.home()
        res.send(data)
    },
    flights: async (req: Request, res: Response) => {
        const data = await Services.flights()
        res.send(data)
    },
    flight: async (req: Request, res: Response) => {
        const data = await Services.flight(req.query)
        res.send(data)
    },
    createFlight: async (req: Request, res: Response) => {
        const data = await Services.saveFlight(req.body)
        res.send(data)
    },
    findFlights: async (req: Request, res: Response) => {
        const data = await Services.OneFlight(req.query)
        res.send(data)
    },
    flightRoundTrip: async (req: Request, res: Response) => {
        const data = await Services.Round(req.query)
        res.send(data)
    },
    multiDestination: async (req: Request, res: Response) => {
        const data = await Services.Multi(req.query)
        res.send(data)
    }
}