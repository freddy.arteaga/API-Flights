import { ISites } from './ISites'
export interface ISegments {
    arrive: ISites,
    depart: ISites,
    city: string,
    country: string,
    duration: string
}